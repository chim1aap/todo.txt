package com.example.todotxt.todo;

import android.annotation.SuppressLint;
import android.os.Build;

import androidx.annotation.RequiresApi;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Character.isAlphabetic;

@RequiresApi(api = Build.VERSION_CODES.KITKAT) // for IsAlphabetic.
public class task {

    private char priority ;
    private String fullLine;
    private Date tVisible ; //of the form t:2020-11-11
    private Date due ; // of the form due:2020-11-11
    Pattern pt   = Pattern.compile(  "t:[0-9]{4}-[0-9]{2}-[0-9]{2}");
    Pattern pDue = Pattern.compile("due:[0-9]{4}-[0-9]{2}-[0-9]{2}");
    private String nakedTask ; // the task without the fluff.

    public task(String fullLine) throws ParseException {
        this.fullLine = fullLine;
        this.priority = extractPriority(fullLine);

        this.tVisible = extractdate(fullLine,pt);
        this.due = extractdate(fullLine, pDue);
    }

    private char extractPriority(String s) {
        String prioritybit = s.substring(0,3) ;
        char[] c = prioritybit.toCharArray();
        if ( c[0] == '(') {
            if(c[2] == ')'){
                // Check if in alphabet:
                if (  isAlphabetic(c[1])){
                    return (c[1]) ;
                }
            }
        }
        //else
        return(0);
    }

    private Date extractdate(String s, Pattern p) throws ParseException {
        Matcher m = p.matcher(s) ;
        Date date = new Date();
        while(m.find()){
            int start = m.start(0);
            int end = m.end(0);
            String dtemp = s.substring(start+2 , end); // to chop off the t: bit
            @SuppressLint("SimpleDateFormat") Date d = new SimpleDateFormat("yyyy-MM-dd").parse(dtemp);
            assert d != null;
            if( d.compareTo(date) > 0  ){
                date = d ;
            }
        }
        return date;
    }
    public char getPriority() {
        return priority;
    }
    // Default stuff
    public String toString(){
        String s = "";
        s += "(" + this.priority +")";
        s += this.fullLine;
        return s;
    }

    // Debug
    public String toDebug(){
        String s = "";
        s += "Prio: "+ this.priority;
        s += "t: "  +this.tVisible;
        s += "due: " + this.due;
        s += "fullLine" + this.fullLine;

        return(s);
    }
}
